package infrastructure

import (
	"encoding/json"
	"os"
)

var infrastructure = new(base)

func Load(file string) error {
	readFile, err := os.ReadFile(file)
	if err != nil {
		return err
	}
	return json.Unmarshal(readFile, infrastructure)
}

type base struct {
	Services *services `json:"services"`
	Kafka    *kafka    `json:"kafka"`
	Mode     *string   `json:"mode"`
}

func Services() ServicesSchema {
	return infrastructure.Services
}

func Kafka() KafkaSchema {
	return infrastructure.Kafka
}

func IsProduction() bool {
	return *infrastructure.Mode == "production"
}

func IsDevelopment() bool {
	return !IsProduction()
}

type services struct {
	Storage_        string `json:"storage"`
	NsfwDetector_   string `json:"nsfw-detector"`
	MediaProcessor_ string `json:"media-processor"`
}

type ServicesSchema interface {
	Storage() string
	NsfwDetector() string
	MediaProcessor() string
}

func (s *services) Storage() string {
	return s.Storage_
}

func (s *services) NsfwDetector() string {
	return s.NsfwDetector_
}

func (s *services) MediaProcessor() string {
	return s.MediaProcessor_
}

type kafka struct {
	MediaProcessorReadTopic_  string `json:"media_processor_read_topic"`
	MediaProcessorWriteTopic_ string `json:"media_processor_write_topic"`
}

type KafkaSchema interface {
	MediaProcessorReadTopic() string
	MediaProcessorWriteTopic() string
}

func (k *kafka) MediaProcessorReadTopic() string {
	return k.MediaProcessorReadTopic_
}

func (k *kafka) MediaProcessorWriteTopic() string {
	return k.MediaProcessorWriteTopic_
}
